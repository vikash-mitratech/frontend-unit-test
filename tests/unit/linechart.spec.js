import { shallowMount } from '@vue/test-utils'
import LineChart from '@/components/LineChart.vue'

describe('LineChart.vue', () => {
    it('renders canvas when passed', () => {
        const wrapper = shallowMount(LineChart)
        
        expect(wrapper.find('canvas'))
        expect(wrapper.find('canvas').classes("chartjs-render-monitor")).toBe(true)
    })

    it('renders correct class with id when passed', () => {
        const wrapper = shallowMount(LineChart)
        
        expect(wrapper.get('#line-chart').get('.chartjs-render-monitor'))
    })

    it('renders correct id with class when passed', () => {
        const wrapper = shallowMount(LineChart)
        
        expect(wrapper.get('.chartjs-render-monitor').get('#line-chart'))
    })

    it('renders height when passed', () => {
        const wrapper = shallowMount(LineChart)
        
        expect(wrapper.find('canvas').attributes('height')).toBe('0')
    })

    it('renders width when passed', () => {
        const wrapper = shallowMount(LineChart)
        
        expect(wrapper.find('canvas').attributes('width')).toBe('0')
    })
})

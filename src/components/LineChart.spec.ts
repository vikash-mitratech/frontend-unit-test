import { mount } from '@cypress/vue'
import LineChart from './LineChart.vue'

describe('LineChart', () => {
  it('get element by id and check its class', () => {
    mount(LineChart, {
      propsData: {
      }
    })

    cy.get('#line-chart').should('have.class', "chartjs-render-monitor")
    cy.get('.chartjs-render-monitor').should('have.id', "line-chart")
  })

  it('get attributes', () => {
    mount(LineChart, {
      propsData: {
      }
    })

    cy.get('canvas').should('have.attr', "height")
    cy.get('canvas').should('have.attr', "width")
  })
})


// it('get screenshots', () => {
//   mount(LineChart, {
//     propsData: {
//     }
//   })

//   cy.get("#line-chart").screenshot('new-screenshot', {
//     onAfterScreenshot($el, props) {
//       // props has information about the screenshot,
//       // including but not limited to the following:
//       // {
//       //   name: 'my-screenshot',
//       //   path: '/Users/janelane/project/screenshots/spec.js/my-screenshot.png',
//       //   size: '15 kb',
//       //   dimensions: {
//       //     width: 1000,
//       //     height: 660,
//       //   },
//       //   scaled: true,
//       //   blackout: [],
//       //   duration: 2300,
//       // }
//       console.log($el, props)
//     },
//   })
// })
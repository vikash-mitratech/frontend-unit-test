import { render } from '@testing-library/vue';
import LineChart from '../src/components/LineChart.vue';

// The case of unit testing for check render msg
test('test in LineChart 1', () => {
    const wrapper = render(LineChart).html()
    console.log(wrapper.includes('line-chart'))
    expect(wrapper).toBeTruthy()
})

test('test in LineChart 2', () => {
    const wrapper = render(LineChart)
    
    const container = document.querySelector('#line-chart')
    
    expect(container.querySelector(".chartjs-render-monitor"))
})

test('test in LineChart 3', () => {
    const wrapper = render(LineChart)
    
    const container = document.querySelector('.chartjs-render-monitor')
    
    expect(container.querySelector("#line-chart"))
})